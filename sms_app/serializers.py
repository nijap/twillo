from sms_app.models import PhoneTrack, MsgTrack, BillingAddress
from rest_framework import serializers


class MsgTrackSerializer(serializers.ModelSerializer):
    class Meta:
        model = MsgTrack
        fields = '__all__'

class PhoneTrackSerializer(serializers.ModelSerializer):
    msgs = MsgTrackSerializer(many=True, read_only=True)
    
    class Meta:
        model = PhoneTrack
        fields = ('phone', 'msgs')


class BillingAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillingAddress
        fields = '__all__'