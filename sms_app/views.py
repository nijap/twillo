import threading
import requests
import json
import stripe
import re, pytracking, time, sched

from django.shortcuts import render, redirect
from twilio.rest import Client
from django.conf import settings
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from sms_app.models import Message, Link, PhoneTrack, MsgTrack
from django.contrib import messages
from datetime import datetime as dt, timedelta
from requests import get
from bs4 import BeautifulSoup

# Create your views here.
from django.views.generic.base import View
from django.contrib.auth.mixins import LoginRequiredMixin
from pytracking import Configuration
from pytracking.django import ClickTrackingView

from allauth.account.views import SignupView
from django.shortcuts import redirect
from django.urls import reverse
from django.db.models import F, Count
from sms_app.serializers import PhoneTrackSerializer, BillingAddressSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework import mixins, status, viewsets
from rest_framework.views import APIView

from .models import BillingAddress

stripe_keys = {
  'secret_key': "sk_test_S675YkxJzlzw9xz6CqkHIKex",
  'publishable_key': "pk_test_nC5yhLMv7EaceuvMSckFCr4q"
}

# stripe_keys = {
#   'secret_key': "sk_test_fYbI0QuWKAXT5rHIn4AJBOwV",
#   'publishable_key': "pk_test_GeaUtAh9Qc1sGxyoXQfcHm0T"
# }



stripe.api_key = stripe_keys['secret_key']

scheduler = sched.scheduler(time.time, time.sleep)
e1 = e2 = None

class CustomSignupView(SignupView):
    http_method_names = ['get']


    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        #print('Hello world')
        messages.success(request, 'Sign up success.')
        return redirect(reverse("login"), permanent=True)


class HomePageView(LoginRequiredMixin, View):
    template_name = 'dashboard.html'

    def get(self, request):
        if request.user.is_superuser:
            return render(request, self.template_name, {'users_data': None})
        try:
            billing_obj = BillingAddress.objects.get(user=request.user)
        except:
            billing_obj = None

        if billing_obj:
            queryset = PhoneTrack.objects.filter(user=request.user)
            serializer = PhoneTrackSerializer(queryset, many=True)
            json = JSONRenderer().render(serializer.data)
            return render(request, self.template_name, {'users_data': serializer.data})
        else:
            return redirect('sms_app:subscribe') 




class SubscribeView(LoginRequiredMixin, APIView):
    template_name = 'subscribe.html'

    def get(self, request):
        try:
            billing_obj = BillingAddress.objects.get(user=request.user)
        except:
            billing_obj = None
        if billing_obj:
            return redirect('sms_app:homepage')             
        else:
            return render(request, self.template_name, {'key': stripe_keys['publishable_key']})

    def post(self, request):
        print(f"========> request.POST: {request.POST}")
        stripe_token = request.POST.get('stripeToken')        
        if stripe_token:
            customer = stripe.Customer.create(
                # email=request.POST['stripeEmail'],
                source=request.POST['stripeToken']
            )

            request.POST._mutable = True
            request.POST['user'] = request.user.id
            request.POST['stripe_id'] = request.POST['stripeToken']
            serializer = BillingAddressSerializer(data=request.POST)
            if serializer.is_valid():
                serializer.save()   

            charge = stripe.Subscription.create(
                customer=customer.id,
                items=[{'plan': 'plan_DPwA9xPMKziC2P'}],
                trial_end=1538472626,
            )

            return redirect('sms_app:homepage') 


class SendMessageView(LoginRequiredMixin, View):
    template_name = 'send_sms.html'

    def get(self, request):
        
        first_message, created = Message.objects.get_or_create(
            user=request.user, 
            msg_type=1,
            defaults={'text_msg': "This is a default FIRST message!", 'send_in': 0},
        )

        second_message, created = Message.objects.get_or_create(
            user=request.user, 
            msg_type=2,
            defaults={'text_msg': "This is a default SECOND message!", 'send_in': 3},
        )

        third_message, created = Message.objects.get_or_create(
            user=request.user, 
            msg_type=3,
            defaults={'text_msg': "This is a default THIRD message!", 'send_in': 8},
        )

        return render(
            request, 
            self.template_name, 
            {'first_msg': first_message, 'second_msg': second_message, 'third_msg': third_message }
            )

    def post(self, request):
        mobile = request.POST.get('mobile')
        first_message = request.POST.get('first_message')
        second_message = request.POST.get('second_message')
        third_message = request.POST.get('third_message')

        
        response = send_msg(request, mobile, first_message, 1)
        if response["success"]:
            messages.success(request, 'Successfully sent your message.')
            schedule(request, mobile, second_message,third_message)
        elif response["exception"]:
            messages.error(request, 'Sorry there is a problem.')
        else:
            messages.info(request, 'Sorry there is a problem.')

        return redirect('sms_app:send_sms') 


def schedule(request, mobile, second_msg, third_msg):
    global e1, e2
    
    second_msg_obj, created = Message.objects.get_or_create(
        user=request.user, 
        msg_type=2,
        defaults={'text_msg': "This is a default SECOND message!", 'send_in': 3},
    )


    third_msg_obj, created = Message.objects.get_or_create(
        user=request.user, 
        msg_type=3,
        defaults={'text_msg': "This is a default THIRD message!", 'send_in': 8},
    )

    sendin_time_snd = dt.now() + timedelta(minutes=second_msg_obj.send_in )
    print(f"sendin_time_snd: {sendin_time_snd}")
    sendin_time_thd = dt.now() + timedelta(minutes=third_msg_obj.send_in+second_msg_obj.send_in)
    print(f"sendin_time_thd: {sendin_time_thd}")

    e1 = scheduler.enterabs(
        time.mktime(sendin_time_snd.timetuple()), 
        1, 
        send_msg, 
        (request, mobile, second_msg, 2)
    )
    e2 = scheduler.enterabs(
        time.mktime(sendin_time_thd.timetuple()), 
        1, 
        send_msg, 
        (request, mobile, third_msg, 3)
    )

    t = threading.Thread(target=scheduler.run)
    t.start()


def shorten(long_link):

    if long_link == None:
        return None

    r = requests.post("https://api.rebrandly.com/v1/links", 
    data = json.dumps({
        "destination": long_link, 
        "domain": { "fullName": "rebrand.ly" }
        # , "slashtag": "A_NEW_SLASHTAG"
        # , "title": "Rebrandly YouTube channel"
    }),
    headers={
        "Content-type": "application/json",
        "apikey": "c7a19e5e0a5c4a2582371e0615ec105f"
    })

    if (r.status_code == requests.codes.ok):
        link = r.json()
        print("Long URL was %s, short URL is %s" % (link["destination"], link["shortUrl"]))
        return link["shortUrl"]
    else:
        raise Exception(r)

def send_msg(request, mobile, msg, msg_type):
    
    #return {"success": True}

    msg_time = dt.now()
    track_click = None
    time_formated = msg_time.strftime('%Y-%m-%d %H:%M:%S')
    print(f"=========> MSG SENT. Schedlued event, user_id: {request.user.id}, mobile: {mobile}, time: {time_formated}, msg_type: {msg_type}")
    #full_mobile = '+212' + mobile

    re_google = re.compile(r'{{ *google_link *}}')
    re_fb = re.compile(r'{{ *facebook_link *}}')
    re_yelp = re.compile(r'{{ *yelp_link *}}')

    phone_track, created = PhoneTrack.objects.get_or_create(user=request.user, phone=mobile)

    if re_google.search(msg):

        google, created = Link.objects.get_or_create(
            user=request.user, 
            link_type="google",
            defaults={'link': "https://www.google.com"},
        )

        google_tracking_url = pytracking.get_click_tracking_url(
            google.link, {"user_id": request.user.id, "phone": mobile, "msg_time": time_formated},
            base_click_tracking_url="http://142.93.10.176/click/",                
            include_webhook_url=False
        )

    
        track_click, created = MsgTrack.objects.update_or_create(
            phone=phone_track,
            msg_type=msg_type,
            msg_time=time_formated,
            defaults={
                'google_short_link':shorten(google_tracking_url),
            }
        )
        
        msg = re_google.sub(track_click.google_short_link, msg)

    elif re_fb.search(msg):
        facebook, created = Link.objects.get_or_create(
            user=request.user, 
            link_type="facebook",
            defaults={'link': "https://www.facebook.com"},
        )

        fb_tracking_url = pytracking.get_click_tracking_url(
            facebook.link, {"user_id": request.user.id, "phone": mobile, "msg_time": time_formated},
            base_click_tracking_url="http://142.93.10.176/click/",                
            include_webhook_url=False
        )
        
        track_click, created = MsgTrack.objects.update_or_create(
            phone=phone_track,
            msg_type=msg_type,
            msg_time=time_formated,
            defaults={
                'fb_short_link': shorten(fb_tracking_url),
            }
        )

        msg = re_fb.sub(track_click.fb_short_link, msg)

    elif re_yelp.search(msg):

        yelp, created = Link.objects.get_or_create(
            user=request.user, 
            link_type="yelp",
            defaults={'link': "https://www.yelp.com"},
        )

        yelp_tracking_url = pytracking.get_click_tracking_url(
            yelp.link, {"user_id": request.user.id, "phone": mobile, "msg_time": time_formated},
            base_click_tracking_url="http://142.93.10.176/click/",                
            include_webhook_url=False
        )

        track_click, created = MsgTrack.objects.update_or_create(
            phone=phone_track,
            msg_type=msg_type,
            msg_time=time_formated,
            defaults={
                'yelp_short_link':shorten(yelp_tracking_url),
            }
        )

        msg = re_yelp.sub(track_click.yelp_short_link, msg)          

    else:
        track_click, created = MsgTrack.objects.get_or_create(
            phone=phone_track,
            msg_type=msg_type,
            msg_time=time_formated
        )


    client = Client(settings.ACCOUNT_SID, settings.AUTH_TOKEN)


    try:
        message = client.messages.create(
            to="2898876997",
            from_=settings.FROM_NUMBER,
            body=msg
        )
        
        if message.sid:
            return {"success": True}
        else:
            return {"success": False, "exception": None}

    except Exception as e:
        print(e)
        return {"success": False, "exception": e}


class EditMessageView(LoginRequiredMixin, View):
    template_name = 'edit_sms.html'

    def get(self, request, handle):
        handle = int(handle)
        if handle == 1:
            msg = "This is a default FIRST message!"
            send_in = 0
        elif handle == 2:
            msg = "This is a default Second message!"
            send_in = 3
        elif handle == 3:
            msg = "This is a default Third message!"
            send_in = 8
        
        msg_obj, created = Message.objects.get_or_create(
            user=request.user, 
            msg_type=handle,
            defaults={'text_msg': msg , 'send_in': send_in},
        )

        context = {'msg': msg_obj.text_msg, 'sendin': msg_obj.send_in, 'handle': handle}
        return render(request, self.template_name, context)

    def post(self, request, handle):
        message = request.POST.get('message')
        sendin = request.POST.get('time')
        msg, created = Message.objects.update_or_create(
            user=request.user, msg_type=handle,
            defaults={'text_msg': message, 'send_in': sendin}
            )
        
        return redirect('sms_app:send_sms')



class SettingsView(LoginRequiredMixin, View):
    template_name = 'settings.html'

    def get(self, request):
        
        google, created = Link.objects.get_or_create(
            user=request.user, 
            link_type="google",
            defaults={'link': "https://www.google.com"},
        )

        facebook, created = Link.objects.get_or_create(
            user=request.user, 
            link_type="facebook",
            defaults={'link': "https://www.facebook.com"},
        )

        yelp, created = Link.objects.get_or_create(
            user=request.user, 
            link_type="yelp",
            defaults={'link': "https://www.yelp.com"},
        )

        context = {'google': google.link, 'facebook': facebook.link, 'yelp': yelp.link}
        return render(request, self.template_name, context)


class EditLinksView(LoginRequiredMixin, View):
    template_name = 'edit_links.html'

    def get(self, request):

        google, created = Link.objects.get_or_create(
            user=request.user, 
            link_type="google",
            defaults={'link': "https://www.google.com"},
        )

        facebook, created = Link.objects.get_or_create(
            user=request.user, 
            link_type="facebook",
            defaults={'link': "https://www.facebook.com"},
        )

        yelp, created = Link.objects.get_or_create(
            user=request.user, 
            link_type="yelp",
            defaults={'link': "https://www.yelp.com"},
        )

        context = {'google': google.link, 'facebook': facebook.link, 'yelp': yelp.link}
        return render(request, self.template_name, context)


    def post(self, request):

        google = request.POST.get('google')
        facebook = request.POST.get('facebook')
        yelp = request.POST.get('yelp')
        

        Link.objects.update_or_create(
            user=request.user, 
            link_type="google",
            defaults={'link': google},
        )

        Link.objects.update_or_create(
            user=request.user, 
            link_type="facebook",
            defaults={'link': facebook},
        )


        Link.objects.update_or_create(
            user=request.user, 
            link_type="yelp",
            defaults={'link': yelp},
        )

        
        return redirect('sms_app:settings')



class MyClickTrackingView(ClickTrackingView):

    def notify_tracking_event(self, tracking_result):
        print(f"######################### TRACKING INFO ##########################")
        print(f"=====> The link {tracking_result.tracked_url} is clicked")
        print(f"=====> The user ip: {tracking_result.request_data['user_ip']}")
        print(f"=====> The user UA: {tracking_result.request_data['user_agent']}")
        print(f"=====> The user phone: {tracking_result.metadata['phone']}")
        print(f"=====> The account id: {tracking_result.metadata['user_id']}")
        print(f"##################################################################")

        url = tracking_result.tracked_url
        re_google = re.compile(r'google')
        re_fb = re.compile(r'facebook')
        re_yelp = re.compile(r'yelp')

        try:
            phone_track = PhoneTrack.objects.get(phone=tracking_result.metadata['phone'])
        except:
            phone_track = None

        if re_google.search(url):
            MsgTrack.objects.filter(
                phone=phone_track,
                msg_time=tracking_result.metadata['msg_time']
            ).update(google_clicks=F('google_clicks') + 1)

        elif re_fb.search(url):
            MsgTrack.objects.filter(
                phone=phone_track,
                msg_time=tracking_result.metadata['msg_time']
            ).update(facebook_clicks=F('facebook_clicks') + 1)

        elif re_yelp.search(url):
            MsgTrack.objects.filter(
                phone=phone_track,
                msg_time=tracking_result.metadata['msg_time']
            ).update(yelp_clicks=F('yelp_clicks') + 1)                    


        if not scheduler.empty():
            try:
                scheduler.cancel(e1)
            except ValueError:
                pass
            
            try:
                scheduler.cancel(e2)
            except ValueError:
                pass
        #send_tracking_result_to_queue(tracking_result)

    def notify_decoding_error(self, exception, request):
        # Called when the tracking link cannot be decoded
        # Override this to, for example, log the exception
        #logger.log(exception)
        print("===========>", exception)

    def get_configuration(self):
        # By defaut, fetchs the configuration parameters from the Django
        # settings. You can return your own Configuration object here if
        # you do not want to use Django settings.
        return Configuration()


class GetRatingView(LoginRequiredMixin, View):
    def get(self, request):
        try:
            link_obj = Link.objects.get(user=request.user, link_type = 'facebook')
            url = link_obj.link
            fb_rating = self.getRating(url, link_obj.link_type)  

            fb_rating_full = int(fb_rating.split('.')[0])
            fb_rating_half = int(fb_rating.split('.')[1])

            fb_empty_stars = 5 - fb_rating_full
            if fb_empty_stars > 1:
                fb_empty_stars = fb_empty_stars - 1
            else:
                fb_empty_stars = 0

            if fb_rating_full == 0:
                fb_rating_zero = 0
            else:
                fb_rating_zero = None

            if fb_rating_half == 0:
                fb_rating_half = 'empty'
            elif fb_rating_half < 5:
                fb_rating_half = 'quarter'
            elif fb_rating_half == 5:
                fb_rating_half = 'half'
            elif fb_rating_half > 5:
                fb_rating_half = 'threefourth' 

            link_yelp_obj = Link.objects.get(user=request.user, link_type = 'yelp')
            url = link_yelp_obj.link

            yelp_rating = self.getRating(url, link_yelp_obj.link_type)   

            yelp_rating_full = int(yelp_rating.split('.')[0])
            yelp_rating_half = int(yelp_rating.split('.')[1])


            yelp_empty_stars = 5 - yelp_rating_full
            if yelp_empty_stars > 1:
                yelp_empty_stars = yelp_empty_stars - 1
            else:
                yelp_empty_stars = 0

            if yelp_rating_full == 0:
                yelp_rating_zero = 0
            else:
                yelp_rating_zero = None

            if yelp_rating_half == 0:
                yelp_rating_half = 'empty'
            elif yelp_rating_half < 5:
                yelp_rating_half = 'quarter'
            elif yelp_rating_half == 5:
                yelp_rating_half = 'half'
            elif yelp_rating_half > 5:
                yelp_rating_half = 'threefourth'

            return HttpResponse(json.dumps({
             'fb_rating_full': fb_rating_full, 'fb_rating_half': fb_rating_half,\
             'fb_rating_zero': fb_rating_zero, 'fb_empty_stars': fb_empty_stars,\
             'yelp_rating_full': yelp_rating_full, 'yelp_rating_half': yelp_rating_half,\
             'yelp_rating_zero': yelp_rating_zero, 'yelp_empty_stars':yelp_empty_stars}))
        except:
            return HttpResponse('error')      

    def getRating(self, url, link_type):
        response = get(url)
        html_soup = BeautifulSoup(response.text, 'html.parser')
        if link_type == 'facebook':
            try:
                rating = html_soup.find('p', class_ = 'accessible_elem').text.split(' ')[0]
            except:
                rating = None
        elif link_type == 'yelp':
            try:
                rating = html_soup.find('img', class_ = 'offscreen')['alt'].split(' ')[0]
            except:
                rating = None
        return rating