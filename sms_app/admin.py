from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(BillingAddress)
admin.site.register(Message)
admin.site.register(Link)
admin.site.register(PhoneTrack)
admin.site.register(MsgTrack)