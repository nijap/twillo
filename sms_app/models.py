from django.db import models

from django.contrib.auth.models import User
# Create your models here.

class Message(models.Model):
    text_msg = models.TextField("Text Message", max_length=600, default="This is a default message!")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    msg_type = models.IntegerField("Message number")
    send_in = models.IntegerField('Send in',default=0)

    def __str__(self):
        return self.text_msg


class Link(models.Model):
    link = models.CharField(max_length=200)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    link_type = models.CharField(max_length=200)


    class Meta:
        unique_together = (('user', 'link_type'),)

    def __str__(self):
        return self.link

class PhoneTrack(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=50)

    def __str__(self):
        return self.phone

class MsgTrack(models.Model):
    phone = models.ForeignKey(PhoneTrack, related_name="msgs", on_delete=models.CASCADE)
    msg_time = models.CharField("Time sent", max_length=100)
    msg_type = models.IntegerField("Message number")
    google_short_link = models.CharField(max_length=200, null=True)
    fb_short_link = models.CharField(max_length=200, null=True)
    yelp_short_link = models.CharField(max_length=200, null=True)
    google_clicks = models.IntegerField("Number of google link clicks", default=0)
    facebook_clicks = models.IntegerField("Number of facebook link clicks", default=0)
    yelp_clicks = models.IntegerField("Number of yelp link clicks", default=0)

    
    class Meta:
        unique_together = (('phone', 'msg_time'),)
        ordering = ['msg_time']

    def __str__(self):
        return f"phone: {self.phone}, msg_type: {self.msg_type}, msg_time: {self.msg_time}."


class BillingAddress(models.Model):
    user = models.ForeignKey(
        User, 
        null=True,
        blank=True,
        on_delete=models.CASCADE
        )
    stripe_id = models.CharField(
        verbose_name=('Stripe ID'),
        max_length=50,
        blank=True,
        )
    address_line1 = models.TextField(
        verbose_name=('Address1'),
        help_text=('Address line 1'),
        blank=True,
    )
    address_line2 = models.TextField(
        verbose_name=('Address2'),
        help_text=('Address line 2'),
        blank=True,
    )
    city = models.CharField(
        verbose_name=('City'),
        max_length=50,
        blank=True,
    )
    state = models.CharField(
        verbose_name=('State'),
        max_length=50,
        blank=True,
    )
    country = models.CharField(
        verbose_name=('Country'),
        max_length=50,
        blank=True,
        default='US',
    )
    zip_code = models.CharField(
        verbose_name=('ZIP code'),
        max_length=20,
        help_text=('ZIP code'),
        blank=True,
    )
    def __str__(self):
        return self.address_line1
