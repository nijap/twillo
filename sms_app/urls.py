from django.urls import path, re_path
from . import views

app_name = "sms_app"

urlpatterns = [
    path('home-page/', views.HomePageView.as_view(), name='homepage'),
    path('send-sms/', views.SendMessageView.as_view(), name='send_sms'),
    path('settings/', views.SettingsView.as_view(), name='settings'),
    path('subscribe/', views.SubscribeView.as_view(), name='subscribe'),
    re_path('^click/(?P<path>[\w=-]+)$', views.MyClickTrackingView.as_view(), name='click_tracking'),
    path('settings/edit-links/', views.EditLinksView.as_view(), name='edit_links'),
    path('edit-sms/<int:handle>/', views.EditMessageView.as_view(), name='edit_sms'),
    path('get-rating/', views.GetRatingView.as_view(), name='get-rating'),
]

